﻿using System;

namespace BFParser
{
	class MainClass
	{
		public static void Main (string[] args)
		{
			string program = Get_Input();
			Parse(program);
			Console.ReadKey();
		}

		public static string Get_Input()
		{
			string input = Console.ReadLine();
			return input;
		}

		public static void Parse(string prog)
		{
			int[] memory;
			memory = new int[30000];
			int pointer = 0;

			for (int i = 0; i < prog.Length; i++)
			{
				switch (prog[i])
				{
					case '+':
						memory[pointer]++;
					break;

					case '-':
						memory[pointer]--;
					break;

					case '<':
						if (pointer != 0)
							pointer--;
						else
							pointer = 30000;
					break;

					case '>':
						if (pointer != 30000)
							pointer++;
						else
							pointer = 0;
					break;

					case '[':
					if(memory[pointer] <= 0)
					{
						while (prog[i]!=']')
						{
							i++;
						}
						i++;
					}
					break;

					case ']':
						while (prog[i] != '[')
						{
							i--;
						}
						i--;
					break;

					case ',':
						memory[pointer] = Int32.Parse(Console.ReadLine());
					break;

					case '.':
						Console.Write(memory[pointer]);
					break;
				}
			}
		}
	}
}
